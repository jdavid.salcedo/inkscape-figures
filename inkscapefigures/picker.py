# A Dmenu wrapper 
import subprocess

class DmenuError(Exception):
    '''The base class for dmenu errors.'''
    pass

class DmenuCommandError(DmenuError):
    '''The dmenu command failed.'''
    def __init__(self, args, error):
        super(DmenuCommandError, self).__init__(
            'The provided dmenu command could not be used (%s): %s' %
            (args, error))

def pick(items,
        command='dmenu',
        centre=None,
        lines=None,
        prompt=None):

    args = [command, '-i', '-nb', '#282a36', '-nf', '#f8f8f2', '-sb', '#bd93f9', '-sf', '#282a36', '-fn', 'Inconsolata:size=16:antialias=true:autohint=true']

    if centre:
        args.append('-c')

    if lines is not None:
        args.extend(('-l', str(lines)))

    if prompt is not None:
        args.extend(('-p', prompt))

    try:
        # start the dmenu process
        proc = subprocess.Popen(
                args,
                universal_newlines=True,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
    except OSError as err:
        # something went wrong with starting the process
        raise DmenuCommandError(args, err)

    # write items over to dmenu
    with proc.stdin:
        for item in items:
            proc.stdin.write(item)
            proc.stdin.write('\n')

    if proc.wait() == 0:
        # user made a selection
        returncode = proc.returncode
        selected = proc.stdout.read().rstrip('\n')

        try:
            index = [it for it in items].index(selected)
        except ValueError:
            index = -1

        if returncode == 0:
            key = 0
        elif returncode == 1:
            key = -1
        elif returncode > 9:
            key = returncode - 9

        return key, index, selected
